-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-10-2021 a las 18:43:54
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pcfacturas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `codigo` int(3) NOT NULL,
  `descripcion` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`codigo`, `descripcion`) VALUES
(1, 'Quesos'),
(2, 'Jamones y Embutidos'),
(3, 'Aceites y Vinagres'),
(4, 'Aperitivos y Frutos Secos'),
(5, 'Conservas y Especias'),
(6, 'Dulces y Mermeladas'),
(7, 'Vinos y Bebidas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `nifcif` varchar(9) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `telefono` varchar(9) DEFAULT NULL,
  `mail` varchar(40) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`nifcif`, `nombre`, `telefono`, `mail`, `direccion`) VALUES
('03892456Z', 'Octavio Ortega Esteb', '633531440', 'octavio@kerchak.com', 'C/ Concilios de Toledo, 2'),
('0394893C', 'Jonathan Jose Chalac', '64323892', 'jj@hotmail.com', 'C/ Lechuga, 45, 4B'),
('4039283B', 'Luis Taboada Rivas', '632678293', 'luistaboada@gmail.com', 'C/ Velada, 12'),
('40839238Y', 'Marta García García', '635890367', 'martagarcia@gmail.com', 'C/ Serranillos, 7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `codigo` int(4) NOT NULL,
  `fecha` date DEFAULT NULL,
  `nifcif` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`codigo`, `fecha`, `nifcif`) VALUES
(1, '2021-10-01', '0394893C'),
(2, '2021-10-02', '4039283B'),
(3, '2021-10-02', '40839238Y'),
(4, '2021-10-02', '4039283B');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineadefactura`
--

CREATE TABLE `lineadefactura` (
  `codigo` int(9) NOT NULL,
  `codigofactura` int(4) DEFAULT NULL,
  `codigoproducto` int(9) DEFAULT NULL,
  `total` decimal(6,2) DEFAULT NULL,
  `cantidad` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `codigoproducto` int(9) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `precioproducto` decimal(6,2) DEFAULT NULL,
  `codigocategoria` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`codigoproducto`, `descripcion`, `precioproducto`, `codigocategoria`) VALUES
(1, 'Queso Manchego Semicurado Novelista 1kg.', '31.35', 1),
(2, 'Jamón de Bellota Ibérico Casalba 8Kg.', '419.00', 2),
(3, 'Aceite de Oliva Virgen Extra Cornicabra El Tilo, 3', '39.00', 3),
(4, 'Jamón de Bellota Puro Extremadura, 8Kg', '479.00', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`nifcif`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `nifcif` (`nifcif`);

--
-- Indices de la tabla `lineadefactura`
--
ALTER TABLE `lineadefactura`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigofactura` (`codigofactura`),
  ADD KEY `codigoproducto` (`codigoproducto`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`codigoproducto`),
  ADD KEY `codigocategoria` (`codigocategoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `codigo` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `codigo` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `lineadefactura`
--
ALTER TABLE `lineadefactura`
  MODIFY `codigo` int(9) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`nifcif`) REFERENCES `clientes` (`nifcif`);

--
-- Filtros para la tabla `lineadefactura`
--
ALTER TABLE `lineadefactura`
  ADD CONSTRAINT `lineadefactura_ibfk_1` FOREIGN KEY (`codigofactura`) REFERENCES `factura` (`codigo`),
  ADD CONSTRAINT `lineadefactura_ibfk_2` FOREIGN KEY (`codigoproducto`) REFERENCES `productos` (`codigoproducto`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`codigocategoria`) REFERENCES `categorias` (`codigo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
