<?php
    class MonstruoDeLasGalletas {
        private $galletas; // Galletas comidas
        public function __construct($f) {
            $this->galletas = 0;
        }
        public function getGalletas() {
            return $this->galletas;
        }
        public function come($g) {
            $this->galletas = $this->galletas + $g;
        }
    }

    class Persona {
        private $nombre;
        private $profesion;
        private $edad;
        //Constructor
        public function __construct($nom, $pro, $edad) {
            $this->nombre = $nom;
            $this->profesion = $pro;
            $this->edad = $edad;
        }
        public function presentarse() {
            return "Hola, me llamo " . $this->nombre . " y soy " . $this->profesion . ".<br>";
        }

        public function __toString() {
            return "<hr><strong>$this->nombre</strong><br>
            Profesion: $this->profesion<br>
            Edad: $this->edad<hr>";
        }
    }

